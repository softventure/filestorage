<?php

namespace Softventure\FileStorage;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\File as LaravelFile;

class FileStorageService
{

    protected $config;
    protected $fileStorageRepository;
    protected $fileStorage;

    const KEY_CONFIG_TEMPORARY_URL_EXPIRY = 'temporary_url_expiry';
    const KEY_CONFIG_ALLOWED_EXTENSIONS = 'allowed_extensions';
    const KEY_CONFIG_FILE_NAME = 'file_storage';
    const KEY_CONFIG_STORAGE_TYPE = 'storage_type';
    const KEY_CONFIG_FILE_METADATA_FORMAT = 'file_metadata_format';
    const DEFAULT_STORAGE_TYPE = 's3';

    public function __construct()
    {
        $this->config = Config::get(FileStorageService::KEY_CONFIG_FILE_NAME);
        $this->fileStorageRepository = new FileStorageRepository();
        $this->fileStorage = Storage::disk($this->getStorageType());
    }

    public function getFiles($parentId = null, $filters=[])
    {
        return $this->getFilesList($parentId, $filters);
    }

    public function createFolder($parent_id, $folderName, $properties=array())
    {
        $folder = new File();
        return $this->setFolderProperties($folder, $parent_id, $folderName, $properties);
    }

    public function updateFolder($folder_id, $parent_id, $folderName, $properties=array(), $filters=array())
    {
        $folder = $this->fileStorageRepository->get($folder_id, $filters);
        return $this->setFolderProperties($folder, $parent_id, $folderName, $properties);
    }

    private function setFolderProperties($folder, $parent_id, $folderName, $properties = array()) {
        $folder->parent_id = $parent_id;
        $folder->name = $folderName;
        foreach ($properties as $property_name => $property_value) {
            $folder->{$property_name} = $property_value;
        }
        return $this->fileStorageRepository->updateFolder($folder);
    }

    /**
     * @param $file_id
     * @return File $file
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getFileWithContent($file_id) {
        $file = $this->fileStorageRepository->get($file_id);
        if (!$file) {
            throw new \Exception(sprintf("File with ID %s not found", $file_id));
        }
        $file->content = $this->fileStorage->get($file->storage_name);
        return $file;
    }

    public function delete($id, $filters=null) {
        $file = $this->fileStorageRepository->get($id, $filters);
        if (!$file) {
            throw new \Exception(sprintf("File with ID %s not found", $file->id));
        }
        if ($file->type == File::TYPE_FILE) {
            $response = $this->fileStorage->delete($file->storage_name);
        }
        return $this->fileStorageRepository->delete($file);
    }

    public function createFile(string $name, string $extension, string $storage_name) {
        $file = new File();
        $file->type = File::TYPE_FILE;
        $file->name = $name;
        $file->extension = $extension;
        $file->storage_name = $storage_name;
        $file->save();
        return $file;
    }

    public function streamUploadFile(File $file, string $filePath, $metadata = array())
    {
        $file_resource = fopen($filePath, "rb");
        $this->validateExtension($file->extension);
        $contentType = mime_content_type($filePath);
        $response = $this->fileStorage->writeStream(
            $file->storage_name,
            $file_resource,
            ['visibility' => 'private', 'ContentType' => $contentType]
        );
        $metadataFile = $this->storeMetadataFile($file->name, $file->storage_name, $metadata);
        if (!$response) {
            throw new \Exception(sprintf("Error: Could not save file '%s' to storage.", $file->name));
        }
        $file = $this->fileStorageRepository->saveFile($file);
        return $file;
    }

    public function uploadFile(File $file, $fileContent, $metadata = array())
    {
        //$file_resource = fopen($file_path, "r");
        $this->validateExtension($file->extension);
        $response = $this->fileStorage->put($file->storage_name, $fileContent);
        $metadataFile = $this->storeMetadataFile($file->name, $file->storage_name, $metadata);
        if (!$response) {
            throw new \Exception(sprintf("Error: Could not save file '%s' to storage.", $file->name));
        }
        $file = $this->fileStorageRepository->saveFile($file);
        return $file;
    }

    private function storeMetadataFile($fileName, $storageFileName, $metadata = array()) {
        $metadataFile = new FileMetadata($fileName, $this->getFileMetadataFormat());
        if (empty($metadata)) {
            $metadata = ['name' => $fileName];
        }
        $metadataFile->setContent($metadata);
        $response = $this->fileStorage->put($metadataFile->getFullFileName($storageFileName), $metadataFile->getFormattedContent());
        return $metadataFile;
    }

    private function getFileMetadataFormat() {
        if (isset($this->config[FileStorageService::KEY_CONFIG_FILE_METADATA_FORMAT])) {
            return $this->config[FileStorageService::KEY_CONFIG_FILE_METADATA_FORMAT];
        }
        return null;
    }

    protected function getFilesList($parentId, $filters)
    {
        $list = $this->fileStorageRepository->getByParentId($parentId, $filters);
        foreach ($list as &$file) {
            if ($file->type == File::TYPE_DIRECTORY) {
                continue;
            }
            $file->download_url = $this->fileStorage->url($file->storage_name);
            $file->temporary_download_url = $this->fileStorage->temporaryUrl($file->storage_name, $this->getMinutes());
        }
        return $list;
    }

    private function getMinutes()
    {
        $now = Carbon::now();
        if (!isset($this->config[FileStorageService::KEY_CONFIG_TEMPORARY_URL_EXPIRY])) {
            return $now->addMinutes(30);
        }
        return $now->addMinutes($this->config[FileStorageService::KEY_CONFIG_TEMPORARY_URL_EXPIRY]);
    }

    private function getAllowedExtensions()
    {
        if (!isset($this->config[FileStorageService::KEY_CONFIG_ALLOWED_EXTENSIONS])) {
            return [];
        }
        return $this->config[FileStorageService::KEY_CONFIG_ALLOWED_EXTENSIONS];
    }

    private function getStorageType()
    {
        if (!isset($this->config[FileStorageService::KEY_CONFIG_STORAGE_TYPE])) {
            return FileStorageService::DEFAULT_STORAGE_TYPE;
        }
        return $this->config[FileStorageService::KEY_CONFIG_STORAGE_TYPE];
    }

    private function validateExtension($extension) {
        $extension = strtolower($extension);
        $prettyAllowedExtensions = $this->getAllowedExtensions() ? implode(", ", $this->getAllowedExtensions()) : "none";
        if (!$extension) {
            throw new \Exception(sprintf('Error: The file has no extension. The allowed extensions are: %s', $prettyAllowedExtensions));
        }
        if (in_array('*', $this->getAllowedExtensions())) {
            return true;
        }
        if (!in_array($extension, $this->getAllowedExtensions())) {
            throw new \Exception(sprintf('The file extension (%s) is not allowed. The allowed extensions are: %s.', $extension, $prettyAllowedExtensions));
        }
        return true;
    }

    public function getStorageNameHash(){
        $uuid = (string) Str::uuid();
        return $uuid;
    }

    public function renameFile($file_id, $parent_id, $fileName, $properties=array(), $filters=array()){
        $file = $this->fileStorageRepository->get($file_id, $filters);
        return $this->setFileProperties($file, $parent_id, $fileName, $properties);
    }

    private function setFileProperties($file, $parent_id, $fileName, $properties=array()){
        $file->parent_id = $parent_id;
        $file->name = $fileName;
        foreach($properties as $property_name => $property_value) {
            $file->{$property_name} = $property_value;
        }
        return $this->fileStorageRepository->saveFile($file);
    }

}
