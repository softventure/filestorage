<?php

return [
    'allowed_extensions' => [],
    'temporary_url_expiry' => 30,
    'file_metadata_format' => 'yml'
];