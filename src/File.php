<?php

namespace Softventure\FileStorage;


use Illuminate\Database\Eloquent\Model;
/**
 * Class File
 * @package Softventure\FileStorage
 * @property integer $id
 * @property integer $parent_id
 * @property string $type
 * @property string $name
 * @property string $extension
 * @property string $storage_name
 * @property string $created_at
 * @property string $updated_at
 */
class File extends Model {

    const TYPE_DIRECTORY = 'directory';
    const TYPE_FILE = 'file';

    protected $table = 'file_storage';

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function parent() {
        return $this->belongsTo(File::class, 'parent_id','id');
    }

    public function children() {
        return $this->hasMany(File::class, 'parent_id','id');
    }
}
