<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_storage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id', false, true)->nullable();
            $table->foreign('parent_id')->references('id')->on('files');
            $table->string('type');
            $table->string('name');
            $table->string('extension')->nullable();
            $table->string('storage_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}