<?php

namespace Softventure\FileStorage;


use Exception;
use Softenture\FileStorage\InvalidFileMetadataFormatException;
use Symfony\Component\Yaml\Yaml;

class FileMetadata {
    /**
     * @var string $name
     */
	protected $name;

    /**
     * @var string $fileType
     */
	protected $fileType;

    /**
     * @var array $content
     */
	protected $content;

    const FILE_TYPE_JSON = 'json';
    const FILE_TYPE_YML = 'yml';
    const DEFAULT_FILE_TYPE = FileMetadata::FILE_TYPE_JSON;

    const ALLOWED_FILE_TYPES = [
		FileMetadata::FILE_TYPE_YML, 
		FileMetadata::FILE_TYPE_JSON
	];

	public function __construct($name, $fileType = null) {
		$this->name = $name;
		$this->fileType = $fileType ? $fileType : FileMetadata::DEFAULT_FILE_TYPE;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function getName() {
		return $this->name;
	}

    /**
     * Returns the file name as "my_file.json"
     * @return string
     */
	public function getFullFileName($storageName) {
		return $storageName . "." . $this->getFileType();
	}

    /**
     * @param $fileType
     * @return $this
     * @throws InvalidFileMetadataFormatException
     */
    public function setFileType($fileType) {
		if (!in_array($fileType, FileMetadata::ALLOWED_FILE_TYPES)) {
			throw new InvalidFileMetadataFormatException(sprintf('The file type %s is not supported. The allowed types are %s.', $fileType, implode(", ", FileMetadata::ALLOWED_FILE_TYPES)));
		}
		$this->fileType = $fileType;
		return $this;
	}

	public function getFileType() {
		return !empty($this->fileType) ? $this->fileType : FileMetadata::DEFAULT_FILE_TYPE;
	}

	public function setContent($content = array()) {
		$this->content = $content;
		return $this;
	}

	public function getContent() {
		return $this->content;
	}

	public function hasContent() {
		return !!$this->content;
	}

    /**
     * @return string
     * @throws Exception
     */
    public function getFormattedContent() {
		if (!$this->hasContent()) {
			throw new Exception("Cannot call format content function when content is null");
		}
		$fileType = $this->getFileType();
		switch ($fileType) {
			case FileMetadata::FILE_TYPE_YML:
				return $this->formatYml($this->getContent());
				break;
			case FileMetadata::FILE_TYPE_JSON:
				return $this->formatJson($this->getContent());
				break;

			default:
				return $this->formatYml($this->getContent());
				break;
		}
	}

	private function formatYml($content) {
		return Yaml::dump($content);
	}

	private function formatJson($content) {
		return json_encode($content);
	}
}
