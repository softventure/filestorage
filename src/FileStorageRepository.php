<?php

namespace Softventure\FileStorage;


class FileStorageRepository {

    public function get($fileId, $filters=array())
    {
        try{
            $file = File::with(['parent', 'children'])->where($filters)->find($fileId);
            if(!$file){
                throw new \Exception (sprintf('Error: No file found with id %s', $fileId));
            }else{
                return $file;
            }
        }catch(\Exception $e){
            throw new \Exception (sprintf('Error: No file found with id %s', $fileId));
        }
    }


    public function getByParentId($parentId, $filters=[])
    {
        $query = File::with('parent', 'children');
        if (!$parentId) {
            $query->whereNull('parent_id');
        } else {
            $query->where('parent_id', '=', $parentId);
        }
        foreach ($filters as $name=>$value) {
            $query = $query->where($name, $value);
        }
        return $query->get();
    }

    public function updateFolder(File $file) {
        $file->type = File::TYPE_DIRECTORY;
        $file->save();
        return $file;
    }

    public function delete($file) {
        if (!$file) {
            throw new \Exception (sprintf('Error: No %s found with id %s', $file->type, $file->id));
        }
        if ($file->type == File::TYPE_DIRECTORY && count($file->children)) {
            throw new \Exception ('Error: The folder is not empty. Please delete or move the files inside and then try again.');
        }
        $file->delete();
        return $file;
    }

    public function saveFile($file) {
        $file->type = File::TYPE_FILE;
        $file->save();
        return $file;
    }

}

